Log for Timbre Talk
 This log is kept with the source code and is refined as the source code changes
 providing a human readable summary of the past, present and future of the code
 base. As issues are encountered, they are entered under the Issues: section. As
 they are resolved or things are changed, they are entered under the Updates:
 section. Once they are no longer relavent, they are removed. The repository will
 contain tags or major milestones which delineate these removals so as to serve
 as a signpost in the source code development preventing the accumulation of clutter.

Issues:
 1. when clicking on send and transfer happens error produced:
 	 ecPacketHandler
     error: structure bigger than list: str=14 list=11
 2. Utility Tab should have list of printme's that can be turned on while running
 3. errors should be caught throughout and put into a log either on a tab or a file
    and or printed to launch window. no error should happen without notifying anyone
 4. when the serial port is bombed, the gui should still function. there should be a
    throttle back option to keep sane
 5. shift base over to pyside to resolve any license issues
 6. generalize code base for Timbre Talk and accept plug ins for:
     protocol, pids, cpuids, targets
 7. make it look prettier on Linux, Windows
 8. make it easy to release a binary for windows and mac
 9. add collaborative max length negotiations for sfp protocol
 10. revise, update and test all code
 11. file button stays blue after loading file
 12. provide the ability to upload from memory and create an SREC file - leave out
     0xFF sequences. Given start and finish.
 13. Phrases should be able to have delays between characters to interface with slow
     links.
 14. add srecord and intel hex file generator
 15. fix popup menu in terminal window so it is not blue
 16. change copied text to be not blue?
 17. should check to make sure pyserial 2.7 or greater is being used
 18. add parity, size and stopbits to monitor utility
 19. change flow of boot loader so that the received bytes come to the next sequence
     so that it makes a decision to move on or not. Same with timeout.
 21. application should be able to load CPU ids and PIDs perhaps handlers too
 22. application should be able to store preferences which would include pids and cpu ids
 23. reset port buffers when changing ports. and perhaps clear once open.
 24. when garbage comes in, displaying in serial hex is fine but if SFP is running, it hangs
 25. change reboot button to send reboot as a command. 4 execute doesn't cut it.

Updates:
 1. removed unneccessary files from folder
 2. modified applist to take arguments
 3. added intel hex records to srecord file interpreter and consolodated to one file
 4. added exception hook to catch all those pesky quiet exceptions and make them visible
 5. added textcolors.py to list all text colors on black and white backgrounds
 6. added support for creating binary applications for the three platforms (Mac,
    Windows, Linux). Follow instructions in "pyinstaller scripts.txt".
 7. Done: 20. need to make bootloader check for file changed on disk
